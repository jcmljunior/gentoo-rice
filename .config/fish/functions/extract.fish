function extract --description 'Descompactador de arquivos.'
	set -l options (fish_opt -s h -l help)
	argparse $options -- $argv

	switch "$argv"

		case '*.tar.bz2'
    	if tar xjf $argv
      	echo "O arquivo foi descompactado com sucesso."
      end

		case '*.tar.gz'
      if tar xzf $argv
				echo "O arquivo foi descompactado com sucesso."
			end

		case '*.tar'
    	if tart xf $argv
      	echo "O arquivo foi descompactado com sucesso."
      end

		case '*.rar'
    	if unrar x $argv
    		echo "O arquivo foi descompactado com sucesso."
    	end

		case '*.zip'
    	if unzip $argv
    		echo "O arquivo foi descompactado com sucesso."
    	end

		case '*.deb'
    	if ar x $argv
      	echo "O arquivo foi descompactado com sucesso."
      end

    case '*'
    	echo "Oppss, sem suporte para esse formato de arquivo."
	end
end
